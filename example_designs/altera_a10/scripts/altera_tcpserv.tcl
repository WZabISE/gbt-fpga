# Open the first Avalon-MM master service
proc jtag_open {} {
    global jtag

    # Close any open service
    if {[info exists jtag(master)]} {
        jtag_close
    }

    set master_paths [get_service_paths master]
    if {[llength $master_paths] == 0} {
        puts "Sorry, no master nodes found"
        return
    }

    # Select the first master service
    set id 0

    foreach name $master_paths {
        if {[string match *phy_0* $name]} { break }
        incr id;
    }

    # Select the first master
    set jtag(master) [lindex $master_paths $id]

    open_service master $jtag(master)

    return
}

proc issp_open { issp_index } {

    global claimed_issp
    
    if {[info exists claimed_issp($issp_index)]} {
        issp_close $issp_index
    }
    
    set issp [lindex [get_service_paths issp] $issp_index]
    set claimed_issp($issp_index) [claim_service issp $issp mylib]
}

# -----------------------------------------------------------------
# Close the JTAG master service
# -----------------------------------------------------------------
#
proc jtag_close {} {
    global jtag

    if {[info exists jtag(master)]} {
        close_service master $jtag(master)
        unset jtag(master)
    }
    return
}
proc issp_close { issp_index } {

    global claimed_issp
    
    close_service master $claimed_issp($issp_index)
}

# -----------------------------------------------------------------
# JTAG-to-Avalon-MM bridge read/write
# -----------------------------------------------------------------
#
proc jtag_read {addr} {
    global jtag
    if {![info exists jtag(master)]} {
        jtag_open
    }

    # Read 32-bits
    set data [master_read_32 $jtag(master) $addr 1]
    return $data
}

proc jtag_write {addr data} {
    global jtag
    if {![info exists jtag(master)]} {
        jtag_open
    }

    # Write 32-bits
    master_write_32 $jtag(master) $addr $data
    return
}

proc read_issp_source { issp_index } {
    global claimed_issp
    
    if {![info exists claimed_issp($issp_index)]} {
        issp_open $issp_index
    }

    set data [issp_read_source_data $claimed_issp($issp_index)]
    return $data
}

proc write_issp_source { issp_index data } {
    global claimed_issp
    
    if {![info exists claimed_issp($issp_index)]} {
        issp_open $issp_index
    }

    issp_write_source_data $claimed_issp($issp_index) $data
    return
}

proc read_issp_probes { issp_index } {
    global claimed_issp
    
    if {![info exists claimed_issp($issp_index)]} {
        issp_open $issp_index
    }

    set data [issp_read_probe_data $claimed_issp($issp_index)]    
    return $data
}

# Copied from Echo_Server example design in Tcl / modif. by EBSM
# LPGBT_SOFT_Server --
#	Open the server listening socket
#	and enter the Tcl event loop
#
# Arguments:
#	port	The server's port number

proc GBTFPGA_SOFT_Server {port} {
    global forever
    set s [socket -server GBTFPGA_SOFTAccept $port]
    vwait forever
}

# LPGBT_SOFT_Accept --
#	Accept a connection from a new client.
#	This is called after a new socket connection
#	has been created by Tcl.
#
# Arguments:
#	sock	The new socket connection to the client
#	addr	The client's IP address
#	port	The client's port number
	
proc GBTFPGA_SOFTAccept {sock addr port} {
    global gbtfpga

    # Record the client's information

    puts "Accept $sock from $addr port $port"
    set gbtfpga(addr,$sock) [list $addr $port]

    # Ensure that each "puts" by the server
    # results in a network transmission

    fconfigure $sock -buffering line

    # Set up a callback for when the client sends data

    fileevent $sock readable [list LPGBT_SOFT $sock]
}

# LPGBT_SOFT --
#	This procedure is called when the server
#	can read data from the client
#
# Arguments:
#	sock	The socket connection to the client

proc LPGBT_SOFT {sock} {
    global gbtfpga
    global forever
    
    set gpio_rt gpio_rt
    set gpio_wt gpio_wt
	
    # Check end of file or abnormal connection drop,
    # then gbtfpga data back to the client.

    if {[eof $sock] || [catch {gets $sock line}] || [string length $line]== 0} {
            
        #close $sock
        #jtag_close
        
        #puts "Close $gbtfpga(addr,$sock)"           
        #unset gbtfpga(addr,$sock)
             
        set forever -1
        
    } else {
		################################ Process Data #################################
		set rcvd_cmd $line
        set ope [lindex $rcvd_cmd 0]
		set addr [lindex $rcvd_cmd 1]
		set data [lindex $rcvd_cmd 2]
		
        switch $ope {
          "r" {
              puts "Read register command"
			  #Rd
			  #set_property CMD.ADDR $addr [get_hw_axi_txns $gpio_rt]
			  #run_hw_axi [get_hw_axi_txns $gpio_rt]
			  #set data [get_property DATA [get_hw_axi_txns $gpio_rt]]
              set data [jtag_read $addr]
              puts [format "Read done %08xh @%08xh" $data $addr]
			  puts $sock $data
		  }
		  
          "w" {
              puts "Write register command"
              #Wr	  
			  #set_property CMD.ADDR $addr [get_hw_axi_txns $gpio_wt]
			  #set_property DATA $data [get_hw_axi_txns $gpio_wt]
			  #run_hw_axi [get_hw_axi_txns $gpio_wt]
              jtag_write $addr $data
              puts [format "Write done %08xh @%08xh" $data $addr]
			  puts $sock 1
		  }          
		  
          "getsources" {
              set data [read_issp_source $addr]
              puts "Get ISSP sources : $data"     
              puts $sock $data
		  } 

          "setsources" {    
              write_issp_source $addr $data
			  puts $sock 1
          }
		  
          "getprobes" {
              set data [read_issp_probes $addr]
              puts "Get ISSP probes : $data"        
			  puts $sock $data
		  }
          
          "ping" {
              puts $sock 1
          }
                    
          default {
            set len [string length $line]
            puts "Command not recognizable: $len - <$line>"
			puts $sock -1
          }
        }
		
		################################# End Process Data #################################
		# puts "Finished data processing"
		# puts $sock $line		
		# puts $line
	}
}

#jtag_write 0x00000000 0x00000001

puts "############## GTFPGA_SOFT - TEST CONTROL ###############"
puts "# Socket Port: 8555                                     #"
puts "# IP Address (localhost): 127.0.0.1                     #"
puts "#########################################################"
            
GBTFPGA_SOFT_Server 8555
vwait forever
