#!/usr/bin/env python

class AlteraISSPSignal():

    def __init__(self, isspaddr, bitPosition, length):
        self.position = bitPosition
        self.size = length
        self.issp = isspaddr

    def getBusPosition(self):
        return self.position

    def getBusSize(self):
        return self.size

    def getISSPAddr(self):
        return self.issp
