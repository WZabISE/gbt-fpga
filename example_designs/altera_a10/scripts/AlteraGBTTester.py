#!/usr/bin/env python
import time
import socket
import sys
import ntpath
import csv
import os
import shutil
from threading  import Thread

from QuartusLIB import QuartusLIB
from AlteraTCPClientLib import AlteraTCPClientLib
from AlteraISSPSignal import AlteraISSPSignal
from GBTFPGAConfLIB import GBTFPGAConfLIB

#Not used in the user example scripts
#from DSO9254A import DSO9254A
#from Latency import LatencyLib

#Configuration
gbtRoot = "D:/GBT-FPGA/svn"
projectPath = "D:/GBT-FPGA/svn/example_designs/altera_a10/alt_gx_devkit/qii_project"
projectName = "devkit_gbt_example_design"

numLinks=[1,1,1,1]
TxOpt=["STANDARD","LATENCY_OPTIMIZED","STANDARD","LATENCY_OPTIMIZED"]
RxOpt=["STANDARD","LATENCY_OPTIMIZED","STANDARD","LATENCY_OPTIMIZED"]
TxEnc=["GBT_FRAME","GBT_FRAME","GBT_FRAME","GBT_FRAME"]
RxEnc=["GBT_FRAME","GBT_FRAME","GBT_FRAME","GBT_FRAME"]
DataPatGen=["ENABLED","ENABLED","ENABLED","ENABLED"]
DataPatCheck=["ENABLED","ENABLED","ENABLED","ENABLED"]
MatchFlag=["ENABLED","ENABLED","ENABLED","ENABLED"]
ClockingScheme=["BC_CLOCK","BC_CLOCK","FULL_MGTFREQ","FULL_MGTFREQ"]

bitFileName = ["Standard_BCClock.sof", "LatOpt_BCClock.sof", "Standard_Unified.sof", "LatOpt_Unified.sof"]

numberOfResets = 500

#numberOfSampleForLatencyMeas = 5000
#scopeIpAddr = "169.254.226.69"
#scopePort = 5025

#Tester
#scope = DSO9254A(scopeIpAddr, scopePort)
#lat = LatencyLib(scope)

for i in range(0, len(bitFileName)):

    print("[Step] %s bitfile generation" %(bitFileName[i]))
    
    #Generate the test package
    gbtFPGAConf = GBTFPGAConfLIB("%s/example_designs/core_sources/exampleDsgn_package.vhd" %(gbtRoot))
    gbtFPGAConf.createConfig(numLinks[i], TxOpt[i], RxOpt[i], TxEnc[i], RxEnc[i], DataPatGen[i], DataPatCheck[i], MatchFlag[i], ClockingScheme[i])
    
    quartus = QuartusLIB(projectPath, projectName)

    if quartus.compile("compile.log", enStatusVerb=False, enInfoVerb=False, enWarningVerb=False) != 0:
        exit(-1)
        
    quartus.timing("timing.log", enStatusVerb=False, enInfoVerb=False, enWarningVerb=False)

    copyBitFile = quartus.copyBitFile(bitFileName[i])

    quartus.program(bitFileName[i], "config.log")

    hwServThread = quartus.runTCPServerInThread("run.log", enStatusVerb=False, enInfoVerb=False, enWarningVerb=False)

    #ISSP signals (defined by the design)
    #   AlteraISSPSignal(ISSP_id, StartBit, BusSize)

    #Probes ISSP 0
    gbtTxReady_from_gbtExamplDsgn = AlteraISSPSignal(0, 0, 1)
    gbtRxReady_from_gbtExamplDsgn = AlteraISSPSignal(0, 1, 1)
    XCVRTXReady_from_gbtExamplDsgn = AlteraISSPSignal(0, 2, 1)
    XCVRRXReady_from_gbtExamplDsgn = AlteraISSPSignal(0, 3, 1)
    XCVRReady_from_gbtExamplDsgn = AlteraISSPSignal(0, 4, 1)
    gbtRxLostFlag_from_gbtExmplDsgn = AlteraISSPSignal(0, 11, 1)
    gbtDataErrSeen_from_gbtExmplDsgn = AlteraISSPSignal(0, 12, 1)
    WBDataErrSeen_from_gbtExmplDsgn = AlteraISSPSignal(0, 13, 1)
    frameclk_locked = AlteraISSPSignal(0, 14, 1)
    countBitsModifiedSignal = AlteraISSPSignal(0, 15, 32)
    countWordReceivedSignal = AlteraISSPSignal(0, 47, 32)

    #Sources ISSP 0
    loopback = AlteraISSPSignal(0, 0, 1)
    rx_pol = AlteraISSPSignal(0, 1, 1)
    tx_pol = AlteraISSPSignal(0, 2, 1)
    reset_dataerrorseenflag = AlteraISSPSignal(0, 3, 1)
    reset_gbtRxReadylostflag = AlteraISSPSignal(0, 4, 1)
    reset_from_issp = AlteraISSPSignal(0, 5, 1)
    resetTx_from_issp = AlteraISSPSignal(0, 6, 1)
    resetRx_from_issp = AlteraISSPSignal(0, 7, 1)
    rxFrameclk_alignPattern = AlteraISSPSignal(0, 8, 6)
    shiftTxClock_from_vio = AlteraISSPSignal(0, 14, 1)
    txShiftCount_from_vio = AlteraISSPSignal(0, 15, 8)
    txPllReset = AlteraISSPSignal(0, 23, 1)
    output_select_from_issp = AlteraISSPSignal(0, 24, 1)

    #Probes ISSP 1
    gbtData_from_gbtExmplDsgn = AlteraISSPSignal(1, 0, 84)
    WBData_from_gbtExmplDsgn = AlteraISSPSignal(1, 84, 116)
    rxIsDataSel_from_issp = AlteraISSPSignal(1, 200, 1)
    rxFrameclkRdy_from_gbtExmplDsgn = AlteraISSPSignal(1, 201, 1)

    #Sources ISSP 1
    gbtData_from_issp = AlteraISSPSignal(1, 0, 84)
    WBData_from_issp = AlteraISSPSignal(1, 84, 116)
    testPattern_from_issp = AlteraISSPSignal(1, 200, 2)
    txIsDataSel_from_issp = AlteraISSPSignal(1, 202, 1)

    #Connect to the TCP server
    connectId = 0        
    while connectId < 10:
        try:
            tcpclient = AlteraTCPClientLib()
            break
        except socket.error, exc:
            connectId = connectId + 1
            time.sleep(1)

    if connectId >= 10:
        exit(-1)

    #Create report file
    latencyMeasFileName = "%s_matchflag_latency.csv" %(ntpath.basename(bitFileName[i])[:-4])

    reportFile = open(latencyMeasFileName, "wb")
    latmeas_file = csv.writer(reportFile)
    latmeas_file.writerow(["Configuration"])
    latmeas_file.writerow([" "])
    latmeas_file.writerow(["Tx Optimization",TxOpt[i]])
    latmeas_file.writerow(["Rx Optimization",RxOpt[i]])
    latmeas_file.writerow(["Tx Encoding",TxEnc[i]])
    latmeas_file.writerow(["Rx Encoding",RxEnc[i]])
    latmeas_file.writerow(["Data Pattern Generator",DataPatGen[i]])
    latmeas_file.writerow(["Data Pattern Checker",DataPatCheck[i]])
    latmeas_file.writerow(["Match flag",MatchFlag[i]])
    latmeas_file.writerow(["Clocking scheme",ClockingScheme[i]])
    latmeas_file.writerow([" "])
    latmeas_file.writerow(["Measurement"])    
    latmeas_file.writerow(["Id","Mean","Minimum","Maximum","Std deviation","Number of samples","Number of bits received","Number of bit corrected","Link B.E.R","Data Error flag"])

    #Test procedure
    for resetId in range(0, numberOfResets):
        
        #Select counter frame
        tcpclient.write_issp(testPattern_from_issp, 1)

        #Reset the core
        tcpclient.write_issp(reset_from_issp, 1)
        gbtRxReady = tcpclient.read_issp(gbtRxReady_from_gbtExamplDsgn)
        if gbtRxReady != 0:
            #Error
            sys.stdout.write("\r                                                                                                     ")
            sys.stdout.write("\r\t\033[1;91m* ERROR: GBT Rx ready still asserted after reset \n\033[0m")
            tcpclient.close()
            hwServThread.join()

            exit(-1)

        time.sleep(1)
        tcpclient.write_issp(reset_from_issp, 0)
        time.sleep(5)

        #Check the Tx frameclock PLL locked status
        TxPLLLocked = tcpclient.read_issp(frameclk_locked)
        if TxPLLLocked != 1:
            #Error
            sys.stdout.write("\r                                                                                                     ")
            sys.stdout.write("\r\t\033[1;91m* ERROR: Tx frameclock PLL is not locked \n\033[0m")
            tcpclient.close()
            hwServThread.join()

            exit(-1)

        #Check the MGT Ready status
        MGTReady = tcpclient.read_issp(XCVRReady_from_gbtExamplDsgn)
        if MGTReady != 1:
            #Error
            sys.stdout.write("\r                                                                                                     ")
            sys.stdout.write("\r\t\033[1;91m* ERROR: MGT is not ready \n\033[0m")
            tcpclient.close()
            hwServThread.join()

            exit(-1)

        #Check GBTRxReady
        GBTRxReady = tcpclient.read_issp(gbtRxReady_from_gbtExamplDsgn)
        if GBTRxReady != 1:
            #Error
            sys.stdout.write("\r                                                                                                     ")
            sys.stdout.write("\r\t\033[1;91m* ERROR: GBT Rx is not ready \n\033[0m")
            tcpclient.close()
            hwServThread.join()

            exit(-1)

        #Measure latency (not implemented in the user example scripts)
#        scope.send(":SYST:HEAD OFF")
#        scope.reset()
                 
#        scope.enableChannel(1, 0.50)
#        scope.enableChannel(3, 0.50)
#        scope.set50DCchannel(1)
#        scope.set50DCchannel(3)

#        scope.send(":CHANNEL1:OFFSET 0")
#        scope.send(":CHANNEL3:OFFSET 0")

#        scope.setTrigger("POS", 0.200, 1)

#        scope.setTimebaseOffset(1e-4)
#        scope.setTimebaseFullScale(2e-4)
#        scope.setSamplingRateMax()

#        sys.stdout.write("\r                                                                                                     \r")
#        latres = lat.measure(1, 3, numberOfSampleForLatencyMeas, resetId, numberOfResets)
#        sys.stdout.write("\r                                                                                                                                           ")
#        sys.stdout.write("\r\t\033[1m* INFO: Mean latency: %0.3fns\n\033[0m" %(float(latres["mean"])*1000000000))

        latres = []
        latres["mean"] = 0
        latres["min"] = 0
        latres["max"] = 0
        latres["std_deviation"] = 0
        latres["count"] = 0
        
        #Count received words: wait for 10^11 bits
        CountReceivedWords = tcpclient.read_issp(countWordReceivedSignal)
        while CountReceivedWords < (100000000000/84):
            CountReceivedWords = tcpclient.read_issp(countWordReceivedSignal)
            time.sleep(1)

        #Error detected?
        ErrorFlag = tcpclient.read_issp(gbtDataErrSeen_from_gbtExmplDsgn)

        #B.E.R before correction: Firmware must be modified to provide this info trhough the ISSP
        CountReceivedWords = tcpclient.read_issp(countWordReceivedSignal)
        BitModifiedCount = tcpclient.read_issp(countBitsModifiedSignal)
        sys.stdout.write("\r                                                                                                     ")
        sys.stdout.write("\r\t\033[1m* INFO: FEC toggled %e bits over %e bits received (Line B.E.R < %e )\n\033[0m" %(BitModifiedCount, (84*CountReceivedWords), (float(1+BitModifiedCount) / float(84*CountReceivedWords))))

        if ErrorFlag == 1:
            #Error
            sys.stdout.write("\r                                                                                                     ")
            sys.stdout.write("\r\t\033[1;91m* ERROR: Data errors have been detected \n\033[0m")
            tcpclient.close()
            hwServThread.join()

            exit(-1)

        latmeas_file.writerow([resetId, float(latres["mean"]), float(latres["min"]), float(latres["max"]), float(latres["std_deviation"]), float(latres["count"]), (84*CountReceivedWords), BitModifiedCount, (float(1+BitModifiedCount) / float(84*CountReceivedWords)), ErrorFlag])
        
    tcpclient.close()
    hwServThread.join()
    reportFile.close()
    
    #Save generated reports
    SaveDirName = "%s_%s" %(projectName, ntpath.basename(bitFileName[i])[:-4])
    if not os.path.exists(SaveDirName):
        os.makedirs(SaveDirName)

    finalOutputDir = "%s/%s" %(SaveDirName, time.strftime("%Y%m%d_%H%M%S"))
    if not os.path.exists(finalOutputDir):
        os.makedirs(finalOutputDir)
    
    shutil.copyfile(latencyMeasFileName, "%s/%s" %(finalOutputDir,latencyMeasFileName))
    shutil.copyfile(bitFileName[i], "%s/bitfile.sof" %(finalOutputDir))
    shutil.copyfile("compile.log", "%s/compile.log" %(finalOutputDir))
    shutil.copyfile("timing.log", "%s/timing.log" %(finalOutputDir))
    shutil.copyfile("config.log", "%s/config.log" %(finalOutputDir))
    shutil.copyfile("run.log", "%s/run.log" %(finalOutputDir))
