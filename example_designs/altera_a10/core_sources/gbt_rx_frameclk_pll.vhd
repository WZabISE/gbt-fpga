library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;


entity gbt_rx_frameclk_pll is
   port ( 

      --=======--
      -- Reset --
      --=======-- 
      RESET_I                                   : in  std_logic;

      --===============--
      -- Clocks scheme --
      --===============--
      RX_WORDCLK_I                              : in  std_logic;  
      FRAMECLK_O                                : out std_logic;    
		
      PLL_LOCKED_O                              : out std_logic
      
   );
end gbt_rx_frameclk_pll;

architecture Behavioral of gbt_rx_frameclk_pll is

	 component rx_frameclk_pll is
		port (
			locked           : out std_logic;                                       -- export
			outclk_0         : out std_logic;                                       -- clk
			refclk           : in  std_logic                    := 'X';             -- clk
			rst              : in  std_logic                    := 'X'              -- reset
		);
	 end component rx_frameclk_pll;
	 
	 signal frameclk: std_logic;
	 
begin
   
   pll_inst: rx_frameclk_pll
		port map(
			locked           => PLL_LOCKED_O,
			outclk_0         => frameclk,
			refclk           => RX_WORDCLK_I,
			rst              => RESET_I
		);
		
	FRAMECLK_O		<= frameclk;
	
end Behavioral;
